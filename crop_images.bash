#!/bin/bash

set -e -o pipefail
set -x

orig_dir="test_imgs/kodak"
rgb_dir="matlab/vq_rgb"
yuv_dir="matlab/vq_yuv"
jpeg_dir="matlab/jpeg"
out_dir="report/imgs"

function crop_img {
    img_num="$1"
    crop_args="$2"
    rotate_args="$3"
    suffix="$4"
    convert "${orig_dir}/${img_num}.bmp" \
	    -crop "$crop_args" \
	    -rotate "${rotate_args}" \
	    "${out_dir}/orig_${img_num}_${suffix}.png"
    convert "${rgb_dir}/kodak_${img_num}.bmp" \
	    -crop "$crop_args" \
	    -rotate "${rotate_args}" \
	    "${out_dir}/rgb_${img_num}_${suffix}.png"
    convert "${yuv_dir}/kodak_${img_num}.bmp" \
	    -crop "$crop_args" \
	    -rotate "${rotate_args}" \
	    "${out_dir}/yuv_${img_num}_${suffix}.png"
    convert "${jpeg_dir}/kodak_${img_num}.bmp" \
	    -crop "$crop_args" \
	    -rotate "${rotate_args}" \
	    "${out_dir}/jpeg_${img_num}_${suffix}.png"
}

mkdir -p "$out_dir"

crop_img 02 1024x1024+0+0 0 full
crop_img 05 140x140+295+0 0 detail
crop_img 13 1024x1024+0+0 0 full
crop_img 13 200x200+547+312 0 detail
crop_img 18 1024x1024+0+0 -90 full
crop_img 18 150x150+478+202 -90 detail
crop_img 23 1024x1024+0+0 0 full
crop_img 23 330x330+210+100 0 detail
