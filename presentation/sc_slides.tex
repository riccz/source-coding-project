\documentclass[]{beamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[english]{babel}

\definecolor{unipdred}{RGB}{155, 0, 20}
\usetheme{CambridgeUS}
\colorlet{darkred}{unipdred} % Overwrite the red from 'beaver'
\setbeamercolor*{structure}{fg=unipdred,bg=white}
\setbeamercolor{alerted text}{fg=darkred!66!red}
\useinnertheme{circles}

\edef\xjobname{\jobname}
\ifdefstring{\xjobname}{sc_notes}{
  \setbeameroption{show only notes}
  %\setbeamertemplate{note page}[plain]
}{
  \setbeameroption{hide notes}
}

\usepackage{codecmds}
\usepackage{mathcmds}

\usepackage[%
  output-product=\cdot,%
  binary-units%
]{siunitx}

\DeclareSIUnit\pixel{{pixel}}
\DeclareSIUnit[per-mode=symbol-or-fraction]\bpp{\bit\per\pixel}

\usepackage{tikz}
\usetikzlibrary{%
  arrows.meta,%
  chains,%
  math,%
  scopes%
}

\def\figwidth{0.5\textwidth}
\def\subfigwidth{0.95\textwidth}

\usepackage{pgfplots}
\pgfplotsset{%
  compat = 1.14,%
  table/search path = {plots}
}
\pgfplotscreateplotcyclelist{my color list}{%
  cyan,%
  orange,%
  red!75!white%
}

\usepackage{graphicx}
\graphicspath{{imgs/}}

\usepackage{import}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{color}
\usepackage{subcaption}
%\usepackage[section, above, below]{placeins}

\usepackage{algpseudocode}
\usepackage{algorithm}

\usepackage{booktabs}

\AtBeginSection[]{
  \begin{frame}
    \begin{block}{}
      \centering%
      \usebeamercolor[fg]{section title}%
      \usebeamerfont{section title}%
      \insertsection
    \end{block}
  \end{frame}
}

\author{Riccardo Zanol}
\date{September 20, 2017}
\title[LBG algorithm for color images]{%
  Implementation of the LBG algorithm \\ for coding color images%
}

\begin{document}
\begin{frame}
  \titlepage
  \note{ In this project we will see the implementation of an
    algorithm based on LBG that allows to compress images by
    quantizing the color values. This algorithm is also compared with
    the JPEG standard.}
\end{frame}

\section*{Objective}
\begin{frame}{Objective of the project}
  \begin{itemize}
  \item Vector quantization: $\SI{24}{\bpp} \to \SI{8}{\bpp}$ (256 colors)
  \item Codebook designed with LBG-split (for each image)
  \item Both RGB and YUV coordinates
  \item Tested on uncompressed photos
  \item Comparison with JPEG
  \end{itemize}
  \note{ More precisely, the objective of this project is to
    vector-quantize the colors of the tested images to reduce their
    rate from \SI{24}{\bpp} to \SI{8}{\bpp}, which means that they
    will be represented using a palette of 256 colors.

    The codebook is chosen separately for each image by applying the
    LBG-split algorithm.  Codebook selection and vector quantization
    is done both on the original RGB coordinates and their conversion
    to YUV.

    These two cases are tested against a set of uncompressed
    photographs of natural scenes with various levels of
    color-richness and amounts of details.

    The results, in terms of distortion, obtained over this image set
    are compared with those given by JPEG.}
\end{frame}

\section{The LBG algorithm}

\begin{frame}{Two versions of LBG}
  \begin{itemize}
  \item There are two versions: known source distribution and
    \alert{unknown source distribution}
  \item The second one is used here
  \item It needs a training set
  \item Unknown color distribution and simpler computations
  \end{itemize}
\note{ The authors of LBG (Linde, Buzo and Gray) give two versions:
  one for the case when the probability distribution of the source is
  known and one for when it is not.

  In the latter case, that is the one used in this project, the
  codebook is based on the statistics of a training set.

  This version is used both because the color distribution is not
  known and also because it requires simpler computations.}
\end{frame}

\begingroup
\algrenewcommand\alglinenumber[1]{\scriptsize #1:}
\begin{frame}{LBG for unknown distributions}
  \begin{block}{}
    \small
    \begin{algorithmic}[1]
      \Require \color{fg} an initial codebook $\set{\vec{y_i}^{(0)}}_{i=1}^K$, a
      training set $T = \set{\vec{x_i}}_{i=1}^N$ and a threshold
      $\epsilon > 0$
      \State $n \gets 1, D^{(0)} \gets +\infty$
      \only<2>{\begingroup\usebeamercolor*[fg]{alerted text}}
      \State $I_i^{(n)} \gets \setst{\vec{x} \in T}{d(\vec{x} - \vec{y_i}^{(n-1)}) \leq
        d(\vec{x} - \vec{y_j}^{(n-1)}) \; \forall j \neq i}, \; i = 1,\dots,K$
      \label{lbg_train:startloop}
      \State $\vec{y_i}^{(n)} \gets \frac{1}{\abs{I_i^{(n)}}}
      \sum_{\vec{x} \in I_i^{(n)}} \vec{x}, \; i=1,\dots,K$
      \label{lbg_train:centroid}
      \only<2>{\endgroup}
      \only<3>{\usebeamercolor*[fg]{alerted text}}
      \State $D^{(n)} \gets \frac{1}{\abs{T}} \sum_{\vec{x} \in T}
      d(\vec{x} - Q(\vec{x}))$
      \label{lbg_train:newD}
      \If {$\frac{D^{(n-1)}-D^{(n)}}{D^{(n)}} < \epsilon$}
      \label{lbg_train:checkD}
      \only<3>{\usebeamercolor*[fg]{normal text}}
      \State \textbf{stop}
      \Else
      \State $n \gets n+1$
      \State \textbf{goto} \ref{lbg_train:startloop}
      \EndIf
    \end{algorithmic}
  \end{block}

  \note<1>{ Here is the pseudocode for the LBG algorithm. This
    algorithm improves iteratively a given initial codebook until it
    finds a local minimum of the distortion.  }

  \note<2>{ LBG first takes the code vectors from the previous
    iteration and optimizes the decision regions for such code vectors.

    Then it computes new code vectors, such that they are optimal for
    the decision regions just computed.  }

  \note<3>{ After that, it checks what is the Mean Squared Error
    introduced by quantizing the training set and how much it has
    decreased with respect to the previous iteration. When a threshold
    is reached the algorithm stops and returns the last codebook it
    computed.}
\end{frame}
\endgroup

\begingroup
\algrenewcommand\alglinenumber[1]{\scriptsize 2.#1:}
\begin{frame}{Handling of the empty regions}
  \only<1>{
    \begin{itemize}
    \item LBG, line 2: { \scriptsize $I_i^{(n)} \gets \setst{\vec{x} \in
        T}{d(\vec{x} - \vec{y_i}^{(n-1)}) \leq d(\vec{x} -
        \vec{y_j}^{(n-1)}) \; \forall j \neq i}, \; i = 1,\dots,K$ }
    \item Some regions can be empty
    \item Few regions per iteration ($\approx 0.009$): use a simple method
    \end{itemize}
  }
  \only<2->{
    \begin{itemize}
    \item Replace line 2 with:
      \begin{block}{}
        \small
        \begin{algorithmic}[1]
          \Loop
          \State $I_i^{(n)} \gets \setst{\vec{x} \in T}{d(\vec{x} - \vec{y_i}^{(n-1)}) \leq
            d(\vec{x} - \vec{y_j}^{(n-1)}) \; \forall j \neq i}, \; i = 1,\dots,K$
          \If {$\exists k$ such that $I_k^{(n)} = \nothing$}
          \only<2>{\usebeamercolor*[fg]{alerted text}}
          \State let $l = \argmax_{i=1,\dots,K
            \text{ such that }\abs{I_i^{(n)}} \geq 2} \set{ \frac{1}{\abs{I_i^{(n)}}}
            \sum_{\vec{x} \in I_i^{(n)}}
            d(\vec{x} - \vec{y_i}^{(n-1)})}$
          \only<2>{\usebeamercolor*[fg]{normal text}}
          \only<3>{\usebeamercolor*[fg]{alerted text}}
          \State let $\vec{\tilde{x}}$ be a randomly chosen element of $I_l^{(n)}$
          \only<3>{\usebeamercolor*[fg]{normal text}}
          \only<4>{\usebeamercolor*[fg]{alerted text}}
          \State $\vec{y_k}^{(n-1)} \gets \vec{\tilde{x}}$
          \only<4>{\usebeamercolor*[fg]{normal text}}
          \Else
          \State \textbf{break loop}
          \EndIf
          \EndLoop
        \end{algorithmic}
    \end{block}
    \end{itemize}
  }

  \note<1>{ This version of LBG may leave some regions empty when they
    are recomputed. This is a waste of resources since there are code
    vectors that are never used.

    To remove the unused code vectors a simple method is used, since
    the occurrence of empty regions is quite rare.}

  \note<2>{ This method consists in selecting the region with highest
    distortion,}

  \note<3>{taking a random vector from it}

  \note<4>{and letting it be a code vector that replaces the empty
    one.}
\end{frame}
\endgroup

\begingroup
\algrenewcommand\alglinenumber[1]{\scriptsize #1:}
\begin{frame}{Building the initial codebook}
  \only<1>{
    \begin{itemize}
    \item LBG needs to start from a given codebook
    \item The \alert{splitting} method is used
    \item Builds iteratively LBG-optimized codebooks of increasing size
    \item Stops when is reaches the desired $K$
    \end{itemize}
  }
  \only<2->{
    \begin{block}{}
      \begin{algorithmic}[1]
        \Require a training set $T = \set{\vec{x_i}}_{i=1}^N$, a codebook
        size $K \leq N$, a perturbation amplitude $\delta > 0$ and an LBG
        threshold $\epsilon > 0$
        \only<5>{\usebeamercolor*[fg]{alerted text}}
        \State generate a random vector $\vec{\epsilon}$
        such that $\norm{\vec{\epsilon}} = \delta$
        \only<5>{\usebeamercolor*[fg]{normal text}}
        \only<2>{\usebeamercolor*[fg]{alerted text}}
        \State $\vec{y_1} \gets \frac{1}{T}\sum_{\vec{x}\in T}\vec{x}, m \gets 1$
        \only<2>{\usebeamercolor*[fg]{normal text}}
        \While {$ 2^m \leq K$}
        \only<3>{\usebeamercolor*[fg]{alerted text}}
        \State $\vec{y_{i+2^{m-1}}} \gets
        \vec{y_i} + \vec{\epsilon}, \; i = 1,\dots,2^{m-1}$
        \only<3>{\usebeamercolor*[fg]{normal text}}
        \If {$2^m \neq K$}
        \only<4>{\usebeamercolor*[fg]{alerted text}}
        \State run LBG on the codebook $\set{\vec{y_i}}_{i=1}^{2^m}$,
        using the training set $T$ and the threshold $\epsilon$
        \only<4>{\usebeamercolor*[fg]{normal text}}
        \State $m \gets m+1$
        \EndIf
        \EndWhile
        \State split the $K - 2^{\floor{\log_2K}}$ code vectors
        associated to the regions with highest average distortion
      \end{algorithmic}
    \end{block}
  }

  \note<1>{ To build the initial codebook that will be optimized by LBG,
    this project uses the splitting method.

    This method consists in building an initial codebook of the
    desired size iteratively by applying LBG on codebooks of
    increasing size. }

  \note<2>{
    This means that, starting from an optimal codebook of size 1,
    which is just the centroid of the training set, }

  \note<3>{ the codebook size is doubled by introducing new vectors
    obtained from the previous ones through the sum with a fixed
    perturbation.}

  \note<4>{
    After the splitting, the codebook is optimized with LBG.

    The splitting and optimization are performed again until the
    codebook has reached the desired size.  }

  \note<5>{ The perturbation is generated randomly, once for each
    image, with a fixed amplitude.}
\end{frame}
\endgroup

\begin{frame}{LBG-split over images}
  \begin{itemize}
    \item Images are just a $H\times W$ matrix of vectors with $L=3$
      components
    \item Training set of $N=HW$ vectors.
    \item LBG-split for the initial codebook
    \item LBG to get the final codebook
  \end{itemize}

  \note{ The application of the LBG and LBG-split algorithms to the
    case of color images is straightforward: the pixels of the image
    constitute the training set.

    First LBG-split is applied to get an initial codebook, then LBG is
    used to optimize it and get the final codebook.}
\end{frame}

\section{The YUV coordinates}

\begin{frame}
  \begin{itemize}
  \item LBG is also applied to the YUV-converted pixels
  \item YUV moves all the brightness information into Y
  \item RGB has it spread over the three components
  \item Used also by JPEG
  \end{itemize}
\note{ In addition to the original RGB coordinates, the LBG-based
  algorithm will be applied also to the YUV ones.

  YUV is a color space used often to represent colors because it
  moves the information about the brightness into a single component
  Y, instead of having it mixed in all three components like is the
  case for RGB.

  It is used also by JPEG.}

\end{frame}

\begin{frame}{The YUV coordinates}
  \begin{itemize}
    \item Just a linear transformation and translation of RGB:
      \begin{equation*}
        \left[\begin{array}{c}
            Y \\ U \\ V
          \end{array}\right] =
        \left[\begin{array}{ccc}
            0.299 & 0.587 & 0.114 \\
            -0.169 & -0.331 & 0.5 \\
            0.5 & -0.419 & -0.081
          \end{array}\right]
        \left[\begin{array}{c}
            R \\ G \\ B
          \end{array}\right] +
        \left[\begin{array}{c}
            0 \\ 127.5 \\ 127.5
          \end{array}\right]
        \label{eq:rgb2yuv}
      \end{equation*}
    \item The translation shifts all components to $[0,255]$
    \item Reverse transformation:
      \begin{equation*}
        \left[\begin{array}{c}
            R \\ G \\ B
          \end{array}\right] =
        \left[\begin{array}{ccc}
            1 & -0.001 & 1.402 \\
            1 & -0.344 & -0.714 \\
            1 & 1.772 & 0.001
          \end{array}\right]
        \left(
        \left[\begin{array}{c}
            Y \\ U \\ V
          \end{array}\right] -
        \left[\begin{array}{c}
            0 \\ 127.5 \\ 127.5
          \end{array}\right]
        \right)
        \label{eq:yuv2rgb}
      \end{equation*}
  \end{itemize}

  \note { The conversion to YUV is just a linear transformation with
    the matrix that can be seen in the slide.  The translation is
    added to shift the U and V components into the same range used by
    the RGB coordinates.

    To reverse this transformation we just need to remove the
    translation and apply the inverse matrix to the YUV components.  }
\end{frame}

\section{Images used for the tests}

\note{Now we see a couple of examples that show what kind of images
  will be used to test the LBG-based algorithm.}

\begin{frame}{Images used for the tests}
  \begin{itemize}
  \item 23 images from the CIPR website (``Kodak'' set): \\
    \url{http://www.cipr.rpi.edu/resource/stills/kodak.html}
  \item Color photographs, all $768\times512$ and \SI{24}{\bpp}
  \item Various kind of images: different performances with LBG and JPEG
  \end{itemize}

  \note{The set of test images consists of 23 color photographs that
    have the same size and rate and were downloaded from the CIPR
    website.

    In this set there are various kind of images that will show which
    kind of photos can be better coded using LBG.
  }
\end{frame}

\begin{frame}{Examples from the test images}
  \only<1>{
    \begin{figure}
      \centering
      \includegraphics[width=90mm]{orig_02_full}
      \caption{Picture 2}
    \end{figure}
  }

  \note<1>{This picture for example is very flat and contains only a
    few colors.  }

  \only<2>{
    \begin{figure}
      \centering
      \includegraphics[width=90mm]{orig_23_full}
      \caption{Picture 23}
    \end{figure}
  }

  \note<2>{This one also contains large flat regions but is very rich
    of color.}

  \only<3>{
    \begin{figure}
      \centering
      \includegraphics[width=90mm]{orig_13_full}
      \caption{Picture 13}
    \end{figure}
  }

  \note<3>{This one, finally, is very rich of small details and
    contains for the most part a set of similar colors (gray, green
    and blue), but has a small detail of a rare color: the yellow
    flowers in the lower-right corner.}
\end{frame}

\section{Choice of the parameters}

\note{Before the LBG-based algorithm can be applied to the test
  images, it is necessary to choose the values for three parameters. }

\begin{frame}{Parameters for LBG and LBG-split}
  \begin{itemize}
  \item Perturbation amplitude:
    $\delta\uncover<2->{=\alert<2>{255\cdot10^{-4}}}$
  \item LBG threshold for splitting:
    $\epsilon'\uncover<2->{=\alert<2>{0.1}}$
  \item LBG threshold for the full-size codebook:
    $\epsilon\uncover<2->{=\alert<2>{10^{-3}}}$
  \item<2-> \alert<2>{Tested separately over the photo set}
  \item<2-> Same values used both for RGB and YUV
  \item<3>One more parameter: \alert<3>{JPEG quantization scaling factor}
  \item<3>Matlab uses 4:2:0 downsampling and standard quantization tables
  \item<3>Scaling controlled by \alert<3>{``quality''}. Set to \alert{95}
  \end{itemize}

\note<1>{ The three parameters are: the perturbation amplitude used to
  split the codebooks, the threshold used in LBG when it is applied to
  the split codebooks and the threshold used in the last execution of
  LBG over the full-size codebook.}

\note<2>{ Each one of the three parameters is tested separately over
  the photo set to determine which values give a lower distortion, while
  not increasing too much the number of LBG iterations required.

  The final choice for the values is shown on the slide and these
  values are used for computing the codebook both in the RGB and in
  the YUV cases.  }

\note<3>{ We still need to set the JPEG parameters.

  The JPEG implementation used, which is the Matlab one,
  always downsamples the chrominance components horizontally and
  vertically and uses the standard quantization tables with a scaling factor.  The
  scaling factor is controlled by the ``quality'' parameter which is
  set to 95 to obtain an average distortion not too far from what is
  given by the LBG-based algorithm.}
\end{frame}

\section{Results}

\note{Now we will see how much distortion is introduced by coding the
  test photos with LBG-split applied to the RGB and YUV components and
  compare it to the distortion introduced by JPEG.}

\begin{frame}{Average results}
  \begin{table}
    \centering
    \begin{tabular}{lccc}
      \toprule
      & RGB & YUV & JPEG \\
      \midrule
      Distortion & \alert<3>{20.14} & \alert<3>{22.27} & \alert<2>{17.12} \\
      Rate \si{[\bpp]} & \alert<4>{8.016} & \alert<4>{8.016} & \alert<2>{3.364} \\
      \bottomrule
    \end{tabular}
  \end{table}
  \begin{itemize}
  \item<2-> On average JPEG is better
  \item<3-> YUV is slightly worse than RGB
  \item<3-> Not surprising: YUV reduces the correlation between components
  \item<4-> The overhead due to the codebook is small: \SI{0.016}{\bpp}
  \end{itemize}

  \note<1>{Here we have the average distortion and rate obtained by
    each of the three methods.}

  \note<2>{We can see immediately that JPEG beats the other two on
    average, as it has both a lower distortion and a lower rate for
    the ``quality'' level that was selected.}

  \note<3>{We can also see that the YUV case is slightly worse than
    the RGB one. This is not a surprise, since the conversion to YUV
    lowers the correlation between the color components.}

  \note<4>{We can finally see that the overhead due to the fact that we
    are using a custom codebook for each image is small.}
\end{frame}

\begin{frame}{Distortion for each picture}
  \pgfplotsset{width = 80mm}
  \only<1>{
    \begin{figure}
      \centering
      \import{plots/}{avgD}
    \end{figure}
  }
  \only<2->{
    \begin{columns}
      \begin{column}{0.5\textwidth}
        \begin{figure}
          \centering
          \resizebox{\textwidth}{!}{
            \import{plots/}{avgD}
          }
        \end{figure}
      \end{column}
      \begin{column}{0.5\textwidth}
        \begin{itemize}
        \item<2-> YUV is always worse than RGB
        \item<3-> Pictures with few colors are distorted less (e.g. 2)
        \item<4-> Pictures with many colors are distorted more (e.g. 23)
        \item<5-> Sometimes LBG beats JPEG in distortion (e.g. 1,2,16,17,20)
        \item<6-> Sometimes LBG is much worse than JPEG (e.g. 5,18,23)
        \end{itemize}
      \end{column}
    \end{columns}
  }

  \note<1>{ Here we can see the distortion introduced in each image by
    each one of the three compression methods. }

  \note<2>{First of all, we can see that the YUV case is worse
    than the RGB one in every case, and not only on average. }

  \note<3>{ Then we can see that the pictures that are distorted less
    by the LBG-based algorithm are those that contain scenes with few
    colors, like the detail of the red door in picture 2.  }

  \note<4>{ Instead, the pictures that are more distorted are those
    with many colors, like picture 23 which shows the two parrots.  }

  \note<5>{ If we compare the distortion obtained in the RGB case with
    the one obtained in the JPEG case, we can see that sometimes the
    LBG-based method beats the JPEG distortion}

  \note<6>{ However there are some cases where the LBG distortion is
    much higher than JPEG

  The cases where LBG and JPEG perform very differently are due to the
  differences in the two algorithms: JPEG exploits the spatial
  correlation and the lack of high-frequency content of flat regions
  to compress them well regardless of their color. LBG instead works
  best when there are not many different colors, even if the image
  contains a lot of energy in the high frequency components.}
\end{frame}

\begin{frame}{LBG codebook for picture 2 (RGB case)}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \begin{subfigure}{\textwidth}
          \centering
          \resizebox{0.9\textwidth}{!}{
            \import{plots/}{cb2rgb}
          }
        \end{subfigure}
        \begin{subfigure}{\textwidth}
          \centering
          \includegraphics[width=0.66\textwidth]{orig_02_full}
        \end{subfigure}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Best case for LBG
      \item Few colors, closely packed
      \item Small decision regions
      \item Can represent many shades of the dominant color
      \end{itemize}
    \end{column}
  \end{columns}

  \note{ Here we can see an example of codebook computed by LBG. This
    is the case of picture 2, which is the less distorted one. We can
    see that it performs so well because the code vectors only need to
    cover a restricted region of the RGB cube, so they can have small
    decision regions and represent many shades of the red present in
    the picture.}
\end{frame}

\begin{frame}{LBG codebook for picture 23 (RGB case)}
  \begin{columns}
    \begin{column}{0.5\textwidth}
            \begin{figure}
        \centering
        \begin{subfigure}{\textwidth}
          \centering
          \resizebox{0.9\textwidth}{!}{
            \import{plots/}{cb23rgb}
          }
        \end{subfigure}
        \begin{subfigure}{\textwidth}
          \centering
          \includegraphics[width=0.66\textwidth]{orig_23_full}
        \end{subfigure}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Worst case for LBG
      \item Colors spread over most of the RGB cube
      \item Large decision regions
      \item Has to approximate lots of colors with the same value
      \end{itemize}
    \end{column}
  \end{columns}

  \note{ Here we have, instead, the worst case for LBG. This time the
    colors are spread much more uniformly over the RGB cube, so the
    code vectors need to be more spaced and have larger decision
    regions to cover them all. This causes a higher distortion than
    the previous case since a larger set of colors needs to be
    approximated with a single one.  }
\end{frame}

\begin{frame}{Visual comparison}
  \begin{itemize}
  \item JPEG coded images: almost indistinguishable in all cases
  \item LBG coded images:
    \begin{itemize}
    \item Almost indistinguishable in the favorable cases
    \item Introduces artifacts: \alert{segmentation} of smooth color
      gradients, high distortion of small details of a \alert{rare
        color}, slight changes of color
    \end{itemize}
  \end{itemize}

  \note{ If we visually compare the decoded images with the original
    ones, we can see almost no difference in the case of JPEG and in
    the most favorable cases of LBG (like picture 2). In most
    pictures, however, LBG introduces some artifacts.

    The most frequent one is the segmentation of smooth color gradients,
    that happens especially with glossy surfaces, and causes them to
    become separated in bands of distinct colors.

    Another kind of artifact that is introduced is the flattening, or
    the deletion in some cases, of details that use a rare color and
    are small but very noticeable.

    Sometimes can also be noticed a slight change of color in some
    regions of the image, but this is unavoidable because we are
    restricting the number of colors. }
\end{frame}

\begin{frame}{Example of segmentation of color gradients from picture 5}
  \begin{figure}
    \centering
    \begin{subfigure}{0.25\textwidth}
      \centering
      \includegraphics[width=\subfigwidth]{orig_05_detail}
      \caption{Original}
    \end{subfigure}%
    \begin{subfigure}{0.25\textwidth}
      \centering
      \includegraphics[width=\subfigwidth]{rgb_05_detail}
      \caption{RGB}
    \end{subfigure}
    \begin{subfigure}{0.25\textwidth}
      \centering
      \includegraphics[width=\subfigwidth]{yuv_05_detail}
      \caption{YUV}
    \end{subfigure}%
    \begin{subfigure}{0.25\textwidth}
      \centering
      \includegraphics[width=\subfigwidth]{jpeg_05_detail}
      \caption{JPEG}
    \end{subfigure}
    %\caption{Red helmet in picture 5.}
  \end{figure}

  \note{ Here we can see an example of the segmentation kind of
    artifact on the red helmet, where it becomes colored in bands of
    red in both the RGB and YUV cases. }
\end{frame}

\begin{frame}{Example of deletion of a small detail from picture 18}
  \begin{figure}
    \centering
    \begin{subfigure}{0.25\textwidth}
      \centering
      \includegraphics[width=\subfigwidth]{orig_18_detail}
      \caption{Original}
    \end{subfigure}%
    \begin{subfigure}{0.25\textwidth}
      \centering
      \includegraphics[width=\subfigwidth]{rgb_18_detail}
      \caption{RGB}
    \end{subfigure}
    \begin{subfigure}{0.25\textwidth}
      \centering
      \includegraphics[width=\subfigwidth]{yuv_18_detail}
      \caption{YUV}
    \end{subfigure}%
    \begin{subfigure}{0.25\textwidth}
      \centering
      \includegraphics[width=\subfigwidth]{jpeg_18_detail}
      \caption{JPEG}
    \end{subfigure}
    %\caption{Red helmet in picture 5.}
  \end{figure}

  \note{ Here instead is an example of a small detail that is
    deleted because is uses a rare color. In this case the LBG
    algorithm does not keep the red of the lipstick in the codebook
    because it is used only by this small region and approximating it
    with a very different color does not worsen the distortion too
    much.  }
\end{frame}

\section*{Conclusion}
\begin{frame}{Conclusion}
  In this project it was:
  \begin{itemize}
  \item Implemented an algorithm to code color images based on LBG-split
  \item Tested the algorithm over a set of photographs
  \item Compared LBG with JPEG
  \end{itemize}
  The results show that
  \begin{itemize}
  \item LBG works better over RGB than YUV
  \item JPEG beats LBG in terms of distortion, rate and visual quality on average
  \item LBG gives a lower distortion in some favorable cases
  \end{itemize}

  \note{ To summarize what was done in this project: it was
    implemented an algorithm based on LBG for coding color images,
    this algorithm was tested on a set of photographs and compared
    with JPEG.

    The results tell us that it is preferable to quantize directly the
    RGB coordinates than to quantize their YUV conversion.  However,
    LBG is not able to match the performances of JPEG in terms of
    distortion, rate or visual quality for the case of photographs of
    natural scenes.  There are only some favorable cases where LBG is
    able to obtain a lower distortion.}
\end{frame}

\end{document}
