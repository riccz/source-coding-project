#!/bin/bash

set -e -o pipefail
set -x

curl 'http://www.cipr.rpi.edu/resource/stills/kodak.html' \
  | grep 'href=.*color.*\.ras' \
  | grep -v 'ftp://' \
  | sed 's/^.*href=\"\(.*\)".*$/\1/' > kodak_urls.txt
wget -c -B 'http://www.cipr.rpi.edu/resource/stills/kodak.html' \
  -P 'test_imgs/kodak' -i kodak_urls.txt

pushd test_imgs/kodak
for i in *.ras; do
    convert "$i" "${i/%.ras/.bmp}"
done
popd

curl 'http://www.cipr.rpi.edu/resource/stills/graphics.html' \
  | grep 'href=.*\.ras' \
  | grep -v 'ftp://' \
  | sed 's/^.*href=\"\(.*\)".*$/\1/' > graphics_urls.txt
wget -c -B 'http://www.cipr.rpi.edu/resource/stills/graphics.html' \
  -P 'test_imgs/graphics' -i graphics_urls.txt

pushd test_imgs/graphics
for i in *.ras; do
    convert "$i" "${i/%.ras/.bmp}"
done
popd
