close all; clear all; clc;

rd_plot = figure();
hold on;
avgD_plot = figure();
hold on;

load('lbg_rgb_data.mat');
figure(rd_plot);
scatter(lbg_rates, lbg_avgDs, 'x');
figure(avgD_plot);
plot(1:length(images), lbg_avgDs, '-O');
writetable(table(lbg_rates.', lbg_avgDs.'), '../report/plots/rgbrd.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', lbg_avgDs.'), '../report/plots/rgbavgD.txt', ...
    'Delimiter', 'space');

total_iters = 0;
total_empty_cells = 0;
for i=1:length(images)
    total_iters = total_iters + lbg_Qs(i).iter_count + lbg_Ss(i).lbg_total_iter_count;
    total_empty_cells = total_empty_cells + lbg_Qs(i).empty_cells_fixed + lbg_Ss(i).empty_cells_fixed;
end

fprintf('RGB: fraction of empty cells = %e\n', total_empty_cells / total_iters);
fprintf('RGB: average distortion = %e\n', mean(lbg_avgDs));
fprintf('RGB: average rate = %e\n', mean(lbg_rates));

load('lbg_yuv_data.mat');
figure(rd_plot);
scatter(lbg_rates, lbg_avgDs, 'x');
figure(avgD_plot);
plot(1:length(images), lbg_avgDs, '-O');
%plot(1:length(images), lbg_avgDs_yuv, '--O');
writetable(table(lbg_rates.', lbg_avgDs.'), '../report/plots/yuvrd.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', lbg_avgDs.'), '../report/plots/yuvavgD.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', lbg_avgDs_yuv.'), '../report/plots/yuvavgDyuv.txt', ...
    'Delimiter', 'space');

total_iters = 0;
total_empty_cells = 0;
for i=1:length(images)
    total_iters = total_iters + lbg_Qs(i).iter_count + lbg_Ss(i).lbg_total_iter_count;
    total_empty_cells = total_empty_cells + lbg_Qs(i).empty_cells_fixed + lbg_Ss(i).empty_cells_fixed;
end

fprintf('YUV: fraction of empty cells = %e\n', total_empty_cells / total_iters);
fprintf('YUV: average distortion = %e\n', mean(lbg_avgDs));
fprintf('YUV: average rate = %e\n', mean(lbg_rates));

load('jpeg_data.mat');
figure(rd_plot);
scatter(jpeg_rates, jpeg_avgDs, 'x');
figure(avgD_plot);
plot(1:length(images), jpeg_avgDs, '-O');
writetable(table(jpeg_rates.', jpeg_avgDs.'), '../report/plots/jpegrd.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', jpeg_avgDs.'), '../report/plots/jpegavgD.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', jpeg_rates.'), '../report/plots/jpegrates.txt', ...
    'Delimiter', 'space');

fprintf('JPEG: average distortion = %e\n', mean(jpeg_avgDs));
fprintf('JPEG: average rate = %e\n', mean(jpeg_rates));

figure(rd_plot);
xlabel('rate');
ylabel('distortion');
legend('RGB', 'YUV', 'JPEG');