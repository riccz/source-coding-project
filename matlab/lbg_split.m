function [codebook, S] = lbg_split(T, K, delta, epsilon)
%lbg_split Compute an initial codebook using LBG-split.
%   Use the training set T to compute an initial codebook of K vectors. At
%   each iteration the codebook vectors are split using a fixed random
%   perturbation of amplitude delta. The LBG algorithm is run with the
%   distortion threshold epsilon.
%   Return the generated codebook and a struct which contains: the number
%   of splits, the number of LBG iterations, the number of empty cells found

assert(K > 0, 'K less than 1');
L = size(T, 1);
assert(epsilon > 0);
assert(delta > 0);

% Use a random perturbation of amplitude delta
perturbation = rand(L, 1);
perturbation = perturbation ./ norm(perturbation) .* delta;

codebook = nan(L, K);
codebook(:,1) = mean(T, 2); % K = 1: centroid of T
S.iter_count = 1;
S.lbg_total_iter_count = 0;
S.empty_cells_fixed = 0;
max_2_power = floor(log2(K));
for i = 1:max_2_power % Split all the vectors up to the max power of 2
    codebook(:, 2^(i-1)+1:2^i) = codebook(:, 1:2^(i-1)) + perturbation;
    
    % If this is not the final K-size codebook run LBG (consider also the
    % additional splits after max_2_power)
    if 2^i ~= K
        Q = lbg(T, codebook(:,1:2^i), epsilon);
        codebook(:,1:2^i) = Q.codebook;
        S.empty_cells_fixed = S.empty_cells_fixed + Q.empty_cells_fixed;
        S.lbg_total_iter_count = S.lbg_total_iter_count + Q.iter_count;
        % if (Q.empty_cells_fixed ~= 0)
        %     warning('Empty cell found while LBG-splitting');
        % end
    end
    S.iter_count = S.iter_count + 1;
end

% Select the missing splits in avg distortion order
if (2^max_2_power ~= K)
    [~, sorted_i] = sort(Q.avgDcells);
    for i = (2^max_2_power+1):K
        i_max = sorted_i(end);
        assert(~isnan(Q.avgDcells(i_max)));
        sorted_i = sorted_i(1:end-1);
        
        codebook(:,i) = codebook(:,i_max) + perturbation;
    end
    S.iter_count = S.iter_count + 1;
end
end
