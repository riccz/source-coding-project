close all; clear all; clc;

figure;
hold on;

T = rand(2, 100) .* 3;
scatter(T(1,:), T(2,:), '.');

cb0 = lbg_split(T, 6, 1e-2, 1e-1);
scatter(cb0(1,:), cb0(2,:), 'O');

Q = lbg(T, cb0, 1e-5);
if (Q.empty_cells_fixed ~= 0)
    warning('Empty cell found in LBG');
end
cb = Q.codebook;
voronoi(cb(1,:), cb(2,:), '*');
