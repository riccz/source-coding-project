close all; clear all; clc;

parpool(32); % Run on the cluster

images = {'01', '02', '03', '04', '05', '06', '07', '08', '09', '10', ...
    '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', ...
    '22', '23'};

K = 256;
split_epsilon = 0.1;
split_delta = 255 * 1e-4;
lbg_epsilon = 1e-3;

lbg_avgDs = nan(1, length(images));
lbg_rates = nan(1, length(images));
lbg_Qs = cell(1,length(images));
lbg_Ss = cell(1,length(images));
lbg_Q_times = nan(1, length(images));
lbg_cb0_times = nan(1, length(images));
lbg_cbs = nan(3,K,length(images));
lbg_cb0s = nan(3,K,length(images));

color_counts = nan(1, length(images));

if ~exist('vq_rgb', 'dir')
    mkdir('vq_rgb');
end

% for i = 1:length(images)
parfor i = 1:length(images)
    img_orig = imread(['../test_imgs/kodak/',images{i}, '.bmp']);
    
    % Count the unique colors
    pixels = reshape(img_orig, [], 1, 3);
    pixels = shiftdim(pixels, 2);
    color_counts(i) = size(unique(transpose(pixels), 'rows'), 1);
    pixels = [];%clear pixels;
    
    % Compute the cb
    [cb, cb0, Q, lbg_cb0_times(i), lbg_Q_times(i), S] = ...
        lbg_split_img(img_orig, K, split_delta, split_epsilon, lbg_epsilon);
    lbg_Qs{i} = Q;
    lbg_Ss{i} = S;
    lbg_cbs(:,:,i) = cb;
    lbg_cb0s(:,:,i) = cb0;
    
    % Encode / Decode
    cb = uint8(cb);
    I_map = vq_encode(cb, img_orig);
    I_q = vq_decode(cb, I_map);
    
    imwrite(uint8(I_q), ['vq_rgb/kodak_', images{i}, '.bmp'], 'bmp');
    
    % Compute the distortion
    lbg_avgDs(i) = avg_distortion(img_orig, I_q);
    
    % Compute the rate
    H = size(I_map, 1);
    W = size(I_map, 2);
    if isa(cb, 'uint8')
        cb_rate = K * 8 * 3 / (W*H);
    elseif isa(cb, 'double')
        cb_rate = K * 64 * 3 / (W*H);
    else
        error('uint8 or double');
    end
    lbg_rates(i) = 8 + cb_rate;
end

lbg_Qs = cell2mat(lbg_Qs);
lbg_Ss = cell2mat(lbg_Ss);

save('lbg_rgb_data.mat');

exit(111); % Check this exit code for successful termination
