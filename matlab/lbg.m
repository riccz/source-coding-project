function Q = lbg(T, codebook, epsilon)
% lbg Optimize a codebook for a training set using the LBG algorithm.
%   T is a LxN matrix of N vectors of dimension L.
%   codebook is a LxK matrix with the initial K codebook vectors.
%   epsilon is the stopping threshold for the relative decrease
%   in avg. distortion.
%
%   Returns the struct Q which contains the optimized codebook,
%   the iteration count, the final average distortion, the map j->i
%   from the training set to the codebook, the
%   average distortion for each cell, the last relative improvement,
%   whether the algorithm converged (has stopped with epsilon = 0),
%   a copy of the initial codebook, the number of empty cells in the final
%   codebook and the number of empty cells that were splitted.

L = size(T, 1);
N = size(T, 2);
assert(size(codebook, 1) == L, 'Codebook vectors have different length');
assert(epsilon > 0);
K = size(codebook, 2);
assert(size(unique(transpose(codebook), 'rows'),1) == K, ...
    'Non-unique initial code vectors');

Q.initial_codebook = codebook;

% Special case: K >= N map each vec from T to one code vec
if K >= N
    % reuse the cb vecs different from training vecs later as empty cells
    cb_not_T = transpose(setdiff(transpose(codebook), ...
        transpose(T), 'rows'));
    % set the cells of size 1
    codebook(:,1:N) = T;
    % set the empty cells
    codebook(:,N+1:K) = cb_not_T(:,1:K-N);
    
    Q.codebook = codebook;
    Q.x2y = 1:N;
    Q.avgD = 0;
    Q.avgDcells = nan(1, K); % empty cells have avg D = NaN
    Q.avgDcells(1:N) = 0;
    Q.iter_count = 0;
    Q.relative_improv = nan;
    Q.converged = true;
    Q.empty_cells_fixed = 0;
    Q.empty_cells = K-N;
    return;
end

Q.empty_cells = 0; % Empty cells at current iteration
Q.empty_cells_fixed = 0; % Total num. of empty cells fixed
Q.iter_count = 1;
D = inf; % distortion at the previous iteration

dists = nan(K, N); % Preallocate to speed up
while true %loop until cb is good enough
    while true %loop until all regions are non-empty
        % squared norms of x_j - y_i
        for i = 1:K
            dists(i,:) = sum((T - codebook(:,i)).^2, 1);
        end
        % decision cell mapping: x2y_j = i if ||y_i - x_j||^2 is min in i
        [~, x2y] = min(dists, [], 1);
        
        % Check for empty cells
        empty_i = setdiff(1:K, x2y);
        if isempty(empty_i)
            break;
        end
        
        % Modify codebook to remove one empty cell, then restart
        codebook = split_maxavgD_cell(T, codebook, dists, x2y, empty_i);
        Q.empty_cells_fixed = Q.empty_cells_fixed + 1;
    end
    
    % update codebook with centroids, compute avg. distortion
    avgDcells = nan(1, K);
    avgD = 0;
    for i = 1:K
        xs = T(:, x2y == i);
        codebook(:,i) = mean(xs, 2);
        % recompute the distances: avg per cell + total avg
        ds = sum((xs - codebook(:,i)).^2, 1);
        avgDcells(i) = mean(ds);
        avgD = avgD + avgDcells(i) * length(xs) / N;
    end
    
    % Check distortion improvement
    assert(~isnan(avgD));
    relative_improv = (D - avgD) / avgD;
    assert(relative_improv >= 0 || avgD == 0, 'Worse distortion');
    if relative_improv <= epsilon || avgD == 0
        break;
    end
    D = avgD;
    if Q.iter_count >= 500
        warning('Force stop after 500 iterations');
        break;
    end
    Q.iter_count = Q.iter_count+1;
end

Q.codebook = codebook;
Q.avgD = avgD;
Q.avgDcells = avgDcells;
Q.converged = relative_improv == 0 || avgD == 0;
Q.x2y = x2y;
Q.relative_improv = relative_improv;
end

function codebook = split_maxavgD_cell(T, codebook, dists, x2y, empty_i)
%split_maxavgD_cell Remove one empty cell by picking a random
%   vector from the cell with higher avg. distortion.

K = size(codebook, 2);

% compute avg. distortion per cell
avgDcells = nan(1, K);
for i = 1:K
    avgDcells(i) = mean(dists(i,x2y == i));
end

% pick the cell with max distortion and more than 1 element
[~, sorted_i] = sort(avgDcells);
sorted_i = flip(sorted_i);
for j = sorted_i
    if ~isnan(avgDcells(j)) % ignore empty cells (avgD == nan)
        sub_T = T(:, x2y == j); % keep the subset that maps to cell j
        if size(sub_T, 2) > 1
            break;
        end
    end
end
assert(size(sub_T, 2) > 1, 'Cannot find a cell to split');

% pick at random in the cell
while true
    new_y = sub_T(:, ceil(rand() * size(sub_T,2)));
    % Check if the vector is already in the codebook
    if all(any(new_y ~= codebook, 1))
        break;
    end
end

% Replace the unused code vector
codebook(:,empty_i(1)) = new_y;
end
