close all; clear all; clc;

load('split_delta_data.mat');

split_deltas = split_deltas ./ 255;

figure;
semilogx(split_deltas, avgavgDs);
xlabel('split delta [*255]');
ylabel('avgD');
writetable(table(split_deltas.', avgavgDs), '../report/plots/avgD_vs_split_delta.txt', ...
    'Delimiter', 'space');


figure;
semilogx(split_deltas, var(avgDs.'));
xlabel('split deltas');
ylabel('varD');


figure;
semilogx(split_deltas, avgsplitlbgiters);
xlabel('split delta [*255]');
ylabel('avg split lbg iters');

figure;
semilogx(split_deltas, avgiters);
xlabel('split delta [*255]');
ylabel('avg iters');

figure;
semilogx(split_deltas, avgiters + avgsplitlbgiters);
xlabel('split delta [*255]');
ylabel('avg total lbg iters');
writetable(table(split_deltas.', avgsplitlbgiters + avgiters), ...
    '../report/plots/iters_vs_split_delta.txt', ...
    'Delimiter', 'space');


figure;
semilogx(split_deltas, var((iters + split_lbg_iters).'));
xlabel('split delta');
ylabel('var total iters');


figure;
semilogx(split_deltas, avgsplittimes);
xlabel('split delta [*255]');
ylabel('avg split time');

figure;
semilogx(split_deltas, avgsplittimes + avgtimes);
xlabel('split delta [*255]');
ylabel('avg total time');

