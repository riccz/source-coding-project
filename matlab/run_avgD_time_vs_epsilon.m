close all; clear all; clc;

parpool(32); % Run on the cluster

images = {'01', '02', '03', '04', '05', '06', '07', '08', '09', '10', ...
    '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', ...
    '22', '23'};

K = 256;
split_epsilon = 0.1;
split_delta = 255 * 1e-2;

lbg_epsilons = logspace(-5, 0, 64);

fprintf('Run on %d lbg_epsilons\n', length(lbg_epsilons));

avgDs = nan(length(lbg_epsilons), length(images));
times = nan(length(lbg_epsilons), length(images));
split_times = nan(length(lbg_epsilons), length(images));
iters = nan(length(lbg_epsilons), length(images));
split_iters = nan(length(lbg_epsilons), length(images));
split_lbg_iters = nan(length(lbg_epsilons), length(images));

parfor j = 1:length(lbg_epsilons)
    avgDs_j = nan(1, length(images));
    times_j = nan(1, length(images));
    split_times_j = nan(1, length(images));
    iters_j = nan(1, length(images));
    split_iters_j = nan(1, length(images));
    split_lbg_iters_j = nan(1, length(images));
    for i = 1:length(images)
        img_orig = imread(['../test_imgs/kodak/',images{i}, '.bmp']);
        
        % Compute the cb
        [cb, ~, Q, s_time, Q_time, S] = ...
            lbg_split_img(img_orig, K, split_delta, split_epsilon, lbg_epsilons(j));
        
        times_j(i) = Q_time;
        split_times_j(i) = s_time;
        iters_j(i) = Q.iter_count;
        split_iters_j(i) = S.iter_count;
        split_lbg_iters_j(i) = S.lbg_total_iter_count;
        
        % Encode / Decode
        I_map = vq_encode(cb, img_orig);
        I_q = vq_decode(cb, I_map);
        
        % Compute the distortion
        avgDs_j(i) = avg_distortion(img_orig, I_q);
    end
    avgDs(j,:) = avgDs_j;
    times(j,:) = times_j;
    split_times(j,:) = split_times_j;
    iters(j,:) = iters_j;
    split_iters(j,:) = split_iters_j;
    split_lbg_iters(j,:) = split_lbg_iters_j;
    fprintf('%d OK\n', j);
end

avgavgDs = mean(avgDs, 2);
avgtimes = mean(times, 2);
avgiters = mean(iters, 2);
avgsplittimes = mean(split_times,2);
avgsplititers = mean(split_iters, 2);
avgsplitlbgiters = mean(split_lbg_iters, 2);

save('avgD_time_vs_epsilon');

exit(111); % Check this exit code for successful termination
