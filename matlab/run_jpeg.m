close all; clear all; clc;

images = {'01', '02', '03', '04', '05', '06', '07', '08', '09', '10', ...
    '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', ...
    '22', '23'};

if ~exist('jpeg', 'dir')
    mkdir('jpeg');
end

jpeg_rates = nan(1, length(images));
jpeg_avgDs = nan(1, length(images));

for i = 1:length(images)
    img_orig = imread(['../test_imgs/kodak/',images{i}, '.bmp']);
    
    % Convert to JPEG
    jpeg_fname = ['jpeg/kodak_', images{i}, '.jpg'];
    imwrite(img_orig, jpeg_fname, 'jpeg', 'Quality', 95);
    
    % Compare with original
    info_jpeg = imfinfo(jpeg_fname);
    jpeg_rates(i) = 8 * info_jpeg.FileSize / ...
        (info_jpeg.Width * info_jpeg.Height);
    img_jpeg = imread(jpeg_fname);
    jpeg_avgDs(i) = avg_distortion(img_orig, img_jpeg);
    
    % Save decoded
    bmp_fname = ['jpeg/kodak_', images{i}, '.bmp'];
    imwrite(img_jpeg, bmp_fname, 'bmp');
end

save('jpeg_data.mat');
