function I_map = vq_encode(codebook, I)
%vq_encode Encode an image I by vector-quantizing each pixel using a given
%   codebook. Outputs the indices of the code vector corresponding to each
%   pixel. The indices are shifted by -1 to fit in a uint8.

L = size(I, 3);
K = size(codebook, 2);
assert(size(codebook, 1) == L);
assert(K <= 256);

H = size(I,1);
W = size(I,2);

% Convert to double before computing dists
I = double(I);
codebook = double(codebook);

% Compute the squared 2-norm of the diff. between
% the image and each code vector
dists = nan(H, W, K);
for i = 1:K
    dists(:,:,i) = sum((I - reshape(codebook(:,i), 1, 1, L)).^2, 3);
end

% Take the vectors with min. distance
[~, I_map] = min(dists, [], 3);
%avgDist = mean(mean(Ds));

% Convert to uint8 and shift the range: 1-256 -> 0-255
I_map = uint8(I_map - 1);
end
