function I = vq_decode(codebook, I_map)
%vq_decode Decode a vector-quantized image.

L = size(codebook, 1);
H = size(I_map, 1);
W = size(I_map, 2);

% Convert to an array
I_map_lin = reshape(I_map, 1, W*H);

% Add +1 to shift the range 0-255 -> 1-256
pixels = codebook(:,uint64(I_map_lin) + 1);

% Convert back to a HxW matrix
I = reshape(shiftdim(pixels, 1), H, W, L);
end
