close all; clear all; clc;

load('split_epsilon_data.mat');

figure;
semilogx(split_epsilons, avgavgDs);
xlabel('split epsilon');
ylabel('avgD');
writetable(table(split_epsilons.', avgavgDs), '../report/plots/avgD_vs_split_epsilon.txt', ...
    'Delimiter', 'space');

figure;
semilogx(split_epsilons, var(avgDs.'));
xlabel('split epsilon');
ylabel('varD');

figure;
semilogx(split_epsilons, avgsplitlbgiters);
xlabel('split epsilon');
ylabel('avg split lbg iters');

figure;
semilogx(split_epsilons, avgiters);
xlabel('split epsilon');
ylabel('avg iters');

figure;
semilogx(split_epsilons, avgiters + avgsplitlbgiters);
xlabel('split epsilon');
ylabel('avg total lbg iters');
writetable(table(split_epsilons.', avgsplitlbgiters + avgiters), ...
    '../report/plots/iters_vs_split_epsilon.txt', ...
    'Delimiter', 'space');

figure;
semilogx(split_epsilons, var((iters + split_lbg_iters).'));
xlabel('split epsilon');
ylabel('var total iters');


figure;
semilogx(split_epsilons, avgsplittimes);
xlabel('split epsilon');
ylabel('avg split time');

figure;
semilogx(split_epsilons, avgsplittimes + avgtimes);
xlabel('split epsilon');
ylabel('avg total time');

