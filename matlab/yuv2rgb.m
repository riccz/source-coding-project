function img_rgb = yuv2rgb(img_yuv)
%YUV2RGB Summary of this function goes here
%   Detailed explanation goes here

persistent Ainv T;
if isempty(Ainv)
    A = [0.299, 0.587, 0.114; ...
        -0.169, -0.331, 0.5; ...
        0.5, -0.419, -0.081];
    Ainv = inv(A);
    T = [0; 127.5; 127.5];
end

img_yuv = double(img_yuv);

Y = img_yuv(:,:,1);
U = img_yuv(:,:,2);
V = img_yuv(:,:,3);

R = Ainv(1,1) * (Y - T(1)) + Ainv(1,2) * (U - T(2)) + Ainv(1,3) * (V - T(3));
G = Ainv(2,1) * (Y - T(1)) + Ainv(2,2) * (U - T(2)) + Ainv(2,3) * (V - T(3));
B = Ainv(3,1) * (Y - T(1)) + Ainv(3,2) * (U - T(2)) + Ainv(3,3) * (V - T(3));

R(R < 0) = 0;
R(R > 255) = 255;
G(G < 0) = 0;
G(G > 255) = 255;
B(B < 0) = 0;
B(B > 255) = 255;

img_rgb = cat(3, R,G,B);
end
