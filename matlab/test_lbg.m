close all; clear all; clc;

cb = [1, 2, 1, 2; 2, 2, 1, 1];
cb(:,5) = [-10;-10];

T = nan(2, 6);
T(:,1) = [0;0];
T(:,2) = [0;3];
T(:,3) = [3;0];
T(:,4) = [1.4;1.4];
T(:,5) = [2.2;1.6];
T(:,6) = [1.5;1.5];

Q = lbg(T, cb, 1e-16);

lbg_cb = Q.codebook;

figure;
hold on;
scatter(cb(1,:), cb(2,:), 'rx');
scatter(T(1,:), T(2,:), 'bO');
scatter(lbg_cb(1,:), lbg_cb(2,:), 'gx');
