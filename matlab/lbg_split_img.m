function [cb, cb0, Q, t_split, t_lbg, S] = lbg_split_img(I, K, delta, ...
    split_epsilon, lbg_epsilon)
%LBG_SPLIT_IMG Compute a codebook for the input image
%   Compute an initial codebook cb0 using lbg_split with parameters delta
%   and split_epsilon. Then compute the final codebook using lbg with
%   parameter lbg_epsilon. Also measure the time needed to split and run
%   LBG

L = size(I, 3);
H = size(I, 1);
W = size(I, 2);
assert(K > 0);
assert(delta > 0);
assert(split_epsilon > 0);
assert(lbg_epsilon > 0);

% Extract the pixels as a HWx3 matrix
pixels = reshape(I, [], 1, 3);
pixels = shiftdim(pixels, 2);
pixels = double(pixels);

% Initial codebook
t1 = tic();
[cb0,S] = lbg_split(pixels, K, delta, split_epsilon);
t_split = toc(t1);

% Build the final codebook
t2 = tic();
Q = lbg(pixels, cb0, lbg_epsilon);
t_lbg = toc(t2);
cb = Q.codebook;

% Check cb vecs are still unique if rounded
cb_u8 = uint8(cb);
if (size(unique(transpose(cb_u8), 'rows'), 1) ~= K)
    warning('Non-unique code vecs after integer approx');
end
end
