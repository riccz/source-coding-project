function img_yuv = rgb2yuv(img_rgb)
%MY_RGB2YUV Summary of this function goes here
%   Detailed explanation goes here

persistent A T;
if isempty(A)
    A = [0.299, 0.587, 0.114; ...
        -0.169, -0.331, 0.5; ...
        0.5, -0.419, -0.081];
    T = [0; 127.5; 127.5];
end

img_rgb = double(img_rgb);

R = img_rgb(:,:,1);
G = img_rgb(:,:,2);
B = img_rgb(:,:,3);

Y = A(1,1) * R + A(1,2) * G + A(1,3) * B + T(1);
U = A(2,1) * R + A(2,2) * G + A(2,3) * B + T(2);
V = A(3,1) * R + A(3,2) * G + A(3,3) * B + T(3);

Y(Y < 0) = 0;
Y(Y > 255) = 255;
U(U < 0) = 0;
U(U > 255) = 255;
V(V < 0) = 0;
V(V > 255) = 255;

img_yuv = cat(3, Y,U,V);
end
