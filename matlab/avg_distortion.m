function avgD = avg_distortion(orig, quant)
%avg_distortion Compute the average square distance between the pixels of
%   the images orig and quant.

diff = double(orig) - double(quant);
D = sum(diff .^ 2, 3);
avgD = mean(mean(D));
end
