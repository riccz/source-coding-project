close all; clear all; clc;

pixs = rand(768, 1024, 3) .* 255;

assert(min(min(min(pixs))) >= 0);
assert(max(max(max(pixs))) <= 255);

%pixs = uint8(pixs);

%figure(); imshow(pixs);

yuvpixs = rgb2yuv(pixs);
pixs_rec = yuv2rgb(yuvpixs);

%pixs_rec = uint8(pixs_rec);

assert(all(all(all(uint8(pixs) == uint8(pixs_rec)))));
max_err = max(max(max(abs(pixs_rec - pixs))));
fprintf('Max err: %e\n', max_err);

r = [1;0;0];
g = [0;1;0];
b = [0;0;1];

ryuv = reshape(rgb2yuv(reshape(r, 1,1,3)), 3, 1);
gyuv = reshape(rgb2yuv(reshape(g, 1,1,3)), 3, 1);
byuv = reshape(rgb2yuv(reshape(b, 1,1,3)), 3, 1);

fprintf('RGB dists RG: %e, RB: %e, GB: %e\n', norm(r-g)^2, norm(r-b)^2, norm(g-b)^2);
fprintf('YUV dists RG: %e, RB: %e, GB: %e\n', norm(ryuv-gyuv)^2, norm(ryuv-byuv)^2, norm(gyuv-byuv)^2);

figure();
hold on;
% plot3([0;r(1)], [0;r(2)], [0;r(3)], 'red--O');
% plot3([0;g(1)], [0;g(2)], [0;g(3)], 'green--O');
% plot3([0;b(1)], [0;b(2)], [0;b(3)], 'blue--O');

plot3([0;ryuv(1)], [0;ryuv(2)], [0;ryuv(3)], 'red');
plot3([0;gyuv(1)], [0;gyuv(2)], [0;gyuv(3)], 'green');
plot3([0;byuv(1)], [0;byuv(2)], [0;byuv(3)], 'blue');


