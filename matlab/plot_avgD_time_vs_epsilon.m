close all; clear all; clc;

load('avgD_time_vs_epsilon.mat');

figure;
semilogx(lbg_epsilons, avgavgDs);
xlabel('epsilon');
ylabel('avgD');
writetable(table(lbg_epsilons.', avgavgDs), '../report/plots/avgD_vs_lbg_epsilon.txt', ...
    'Delimiter', 'space');


figure;
semilogx(lbg_epsilons, var(avgDs.'));
xlabel('epsilon');
ylabel('varD');

figure;
semilogx(lbg_epsilons, avgiters);
xlabel('epsilon');
ylabel('avg iters');

figure;
semilogx(lbg_epsilons, avgsplitlbgiters);
xlabel('epsilon');
ylabel('avg split lbg iters');

figure;
semilogx(lbg_epsilons, avgsplitlbgiters + avgiters);
xlabel('epsilon');
ylabel('avg total lbg iters');
writetable(table(lbg_epsilons.', avgsplitlbgiters + avgiters), ...
    '../report/plots/iters_vs_lbg_epsilon.txt', ...
    'Delimiter', 'space');


figure;
semilogx(lbg_epsilons, var((iters + split_lbg_iters).'));
xlabel('epsilon');
ylabel('var total iters');



figure;
semilogx(lbg_epsilons, avgtimes);
xlabel('epsilon');
ylabel('avg time');

figure;
semilogx(lbg_epsilons, avgtimes + avgsplittimes);
xlabel('epsilon');
ylabel('avg total time');
