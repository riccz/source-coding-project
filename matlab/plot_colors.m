close all; clear all; clc;

load('lbg_rgb_data.mat');

cc_plot = figure();
hold on;
plot(1:length(images), color_counts, '-O');

rgb_cc = color_counts;

cb = lbg_Qs(2).codebook;
figure();
scatter3(cb(1,:),cb(2,:),cb(3,:), 64, shiftdim(cb, 1)./255, 'fill');
xlabel('R'); ylabel('G'); zlabel('B');

cb = lbg_Qs(2).codebook.';
cb_r = round(cb(:,1) ./ 255, 3);
cb_g = round(cb(:,2) ./ 255, 3);
cb_b = round(cb(:,3) ./ 255, 3);
writetable(table(cb, cb_r, cb_g, cb_b, ...
    'VariableName', {'cb2', 'R', 'G', 'B'}), '../report/plots/cb2_rgb.txt', ...
    'Delimiter', 'space');
cb = lbg_Qs(23).codebook.';
cb_r = round(cb(:,1) ./ 255, 3);
cb_g = round(cb(:,2) ./ 255, 3);
cb_b = round(cb(:,3) ./ 255, 3);
writetable(table(cb, cb_r, cb_g, cb_b, ...
    'VariableName', {'cb23', 'R', 'G', 'B'}), '../report/plots/cb23_rgb.txt', ...
    'Delimiter', 'space');

load('lbg_yuv_data.mat');
figure(cc_plot);
plot(1:length(images), color_counts, '-O');

assert(all(all(color_counts == rgb_cc)));
writetable(table((1:length(images)).', color_counts.'), '../report/plots/cc.txt', ...
    'Delimiter', 'space');

cb = lbg_Qs(2).codebook;
cb_rgb = yuv2rgb(reshape(shiftdim(cb, 1), K, 1, 3));
figure();
scatter3(cb_rgb(:,1),cb_rgb(:,2),cb_rgb(:,3), 64, reshape(cb_rgb,K,3) ./255, 'fill');
xlabel('R'); ylabel('G'); zlabel('B');

cb = reshape(yuv2rgb(reshape(shiftdim(lbg_Qs(2).codebook,1), K, 1, 3)), K, 3);
cb_r = round(cb(:,1) ./ 255, 3);
cb_g = round(cb(:,2) ./ 255, 3);
cb_b = round(cb(:,3) ./ 255, 3);
writetable(table(cb, cb_r, cb_g, cb_b, ...
    'VariableName', {'cb2', 'R', 'G', 'B'}), '../report/plots/cb2_yuv.txt', ...
    'Delimiter', 'space');
cb = reshape(yuv2rgb(reshape(shiftdim(lbg_Qs(23).codebook,1), K, 1, 3)), K, 3);
cb_r = round(cb(:,1) ./ 255, 3);
cb_g = round(cb(:,2) ./ 255, 3);
cb_b = round(cb(:,3) ./ 255, 3);
writetable(table(cb, cb_r, cb_g, cb_b, ...
    'VariableName', {'cb23', 'R', 'G', 'B'}), '../report/plots/cb23_yuv.txt', ...
    'Delimiter', 'space');
