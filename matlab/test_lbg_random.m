close all; clear all; clc;

T = rand(2, 2048) .* 3;
cb = rand(2, 256) .* 3;
%cb = [1, 2, 1, 2; 2, 2, 1, 1];


tic;
Q = lbg(T, cb, 1e-4);
toc;

lbg_cb = Q.codebook;

figure;
hold on;


for i = 1:length(cb)
    %scatter(T(1,x2y == i), T(2,x2y==i), 'O');
end
%scatter(cb(1,:), cb(2,:), 'bx');
scatter(lbg_cb(1,:), lbg_cb(2,:), 'rx');
