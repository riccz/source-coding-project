close all; clear all; clc;

images = {'01', '02', '03', '04', '05', '06', '07', '08', '09', '10', ...
    '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', ...
    '22', '23'};

cov_rgb = nan(length(images),3,3);
cov_yuv = nan(length(images),3,3);

for i = 1:length(images)
    img_rgb = double(imread(['../test_imgs/kodak/',images{i}, '.bmp']));
    img_yuv = rgb2yuv(img_rgb);
    img_rgb2 = yuv2rgb(img_yuv);
    avgD = avg_distortion(img_rgb,img_rgb2);
    assert(avgD < 1e-4);
    
    pixels_rgb = reshape(img_rgb, [], 1, 3);
    pixels_rgb = shiftdim(pixels_rgb, 2);
    pixels_yuv = reshape(img_yuv, [], 1, 3);
    pixels_yuv = shiftdim(pixels_yuv, 2);
    
    cov_rgb(i,1,1) = xcov(pixels_rgb(1,:), pixels_rgb(1,:), 0);
    cov_rgb(i,1,2) = xcov(pixels_rgb(1,:), pixels_rgb(2,:), 0);
    cov_rgb(i,1,3) = xcov(pixels_rgb(1,:), pixels_rgb(3,:), 0);
    cov_rgb(i,2,1) = cov_rgb(i,1,2);
    cov_rgb(i,2,2) = xcov(pixels_rgb(2,:), pixels_rgb(2,:), 0);
    cov_rgb(i,2,3) = xcov(pixels_rgb(2,:), pixels_rgb(3,:), 0);
    cov_rgb(i,3,1) = cov_rgb(i,1,3);
    cov_rgb(i,3,2) = cov_rgb(i,2,3);
    cov_rgb(i,3,3) = xcov(pixels_rgb(3,:), pixels_rgb(3,:), 0);
    
    cov_yuv(i,1,1) = xcov(pixels_yuv(1,:), pixels_yuv(1,:), 0);
    cov_yuv(i,1,2) = xcov(pixels_yuv(1,:), pixels_yuv(2,:), 0);
    cov_yuv(i,1,3) = xcov(pixels_yuv(1,:), pixels_yuv(3,:), 0);
    cov_yuv(i,2,1) = cov_yuv(i,1,2);
    cov_yuv(i,2,2) = xcov(pixels_yuv(2,:), pixels_yuv(2,:), 0);
    cov_yuv(i,2,3) = xcov(pixels_yuv(2,:), pixels_yuv(3,:), 0);
    cov_yuv(i,3,1) = cov_yuv(i,1,3);
    cov_yuv(i,3,2) = cov_yuv(i,2,3);
    cov_yuv(i,3,3) = xcov(pixels_yuv(3,:), pixels_yuv(3,:), 0);
end

varR = cov_rgb(:,1,1);
varG = cov_rgb(:,2,2);
varB = cov_rgb(:,3,3);

varY = cov_yuv(:,1,1);
varU = cov_yuv(:,2,2);
varV = cov_yuv(:,3,3);

figure;
hold on;
plot(1:length(images), varR, '-O');
plot(1:length(images), varG, '-O');
plot(1:length(images), varB, '-O');
plot(1:length(images), varY, '-O');
plot(1:length(images), varU, '-O');
plot(1:length(images), varV, '-O');
legend('R', 'G', 'B', 'Y', 'U', 'V');
ylabel('Power');
xlabel('Picture');

figure;
hold on;
plot(1:length(images), cov_rgb(:,1,2) ./ sqrt(varR) ./ sqrt(varG), '-O');
plot(1:length(images), cov_rgb(:,1,3) ./ sqrt(varR) ./ sqrt(varB), '-O');
plot(1:length(images), cov_rgb(:,2,3) ./ sqrt(varG) ./ sqrt(varB), '-O');
legend('RG', 'RB', 'GB', 'Location', 'southwest');
ylabel('Normalized correlation');
xlabel('Picture');
%ylim([0.7,1]);

writetable(table((1:length(images)).', (cov_rgb(:,1,2) ./ sqrt(varR) ./ sqrt(varG))), ...
    '../report/plots/covRG.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', (cov_rgb(:,1,3) ./ sqrt(varR) ./ sqrt(varB))), ...
    '../report/plots/covRB.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', (cov_rgb(:,2,3) ./ sqrt(varG) ./ sqrt(varB))), ...
    '../report/plots/covGB.txt', ...
    'Delimiter', 'space');

figure;
hold on;
plot(1:length(images), cov_yuv(:,1,2) ./ sqrt(varY) ./ sqrt(varU), '-O');
plot(1:length(images), cov_yuv(:,1,3) ./ sqrt(varY) ./ sqrt(varV), '-O');
plot(1:length(images), cov_yuv(:,2,3) ./ sqrt(varU) ./ sqrt(varV), '-O');
legend('YU', 'YV', 'UV', 'Location', 'southwest');
ylabel('Normalized correlation');
xlabel('Picture');
%ylim([0.7,1]);

writetable(table((1:length(images)).', (cov_yuv(:,1,2) ./ sqrt(varY) ./ sqrt(varU))), ...
    '../report/plots/covYU.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', (cov_yuv(:,1,3) ./ sqrt(varY) ./ sqrt(varV))), ...
    '../report/plots/covYV.txt', ...
    'Delimiter', 'space');
writetable(table((1:length(images)).', (cov_yuv(:,2,3) ./ sqrt(varU) ./ sqrt(varV))), ...
    '../report/plots/covUV.txt', ...
    'Delimiter', 'space');
