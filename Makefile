all: sc_project.zip

CODE_FILES = run_lbg_*.m run_jpeg.m
CODE_FILES += run_avgD_*.m run_vs_split_*.m
CODE_FILES += avg_distortion.m rgb2yuv.m yuv2rgb.m
CODE_FILES += lbg.m lbg_split.m lbg_split_img.m
CODE_FILES += plot_avgD_*.m plot_vs_split_*.m
CODE_FILES += plot_colors.m plot_results.m
CODE_FILES += vq_encode.m vq_decode.m
CODE_FILES += compute_correlation.m

BUILDDIR = build/sc_project

sc_project.zip: clean
	./crop_images.bash
	$(MAKE) -C report pdf
	$(MAKE) -C presentation pdf
	mkdir -p $(BUILDDIR)
	cp report/sc_report.pdf $(BUILDDIR)/
	cp presentation/sc_slides.pdf $(BUILDDIR)/
	cp presentation/sc_notes.pdf $(BUILDDIR)/
	mkdir -p $(BUILDDIR)/matlab
	cd matlab && cp $(CODE_FILES) ../$(BUILDDIR)/matlab/
	mkdir -p $(BUILDDIR)/test_imgs/kodak
	cp test_imgs/kodak/*.bmp $(BUILDDIR)/test_imgs/kodak/
	mkdir -p $(BUILDDIR)/test_imgs/vq_rgb
	cp matlab/vq_rgb/*.bmp $(BUILDDIR)/test_imgs/vq_rgb/
	mkdir -p $(BUILDDIR)/test_imgs/vq_yuv
	cp matlab/vq_yuv/*.bmp $(BUILDDIR)/test_imgs/vq_yuv/
	mkdir -p $(BUILDDIR)/test_imgs/jpeg
	cp matlab/jpeg/*.bmp $(BUILDDIR)/test_imgs/jpeg/
	cd build && zip -9 -r sc_project.zip sc_project/
	cp build/sc_project.zip ./

clean:
	$(MAKE) -C report clean
	$(MAKE) -C presentation clean
	rm -r build || true

.PHONY: all clean
